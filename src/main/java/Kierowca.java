import java.io.Serializable;

public class Kierowca implements Serializable {
    private static final long serialVersionUID = 1L;
    private String imie;
    private String nazwisko;
    private String miejscowosc;
    private Long rokUrodzenia;

    public Kierowca(String imie, String nazwisko, String miejscowosc, Long rokUrodzenia) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.miejscowosc = miejscowosc;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String getImie(){
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
    public String getMiejscowosc() {
        return miejscowosc;
    }
    public Long getRokUrodzenia() {
        return rokUrodzenia;
    }

    public void setImie(String imie) {

        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {

        this.nazwisko = nazwisko;
    }
    public void setMiejscowosc(String miejscowosc) {

        this.miejscowosc = miejscowosc;
    }
    public void setRokUrodzenia(Long rokUrodzenia) {

        this.rokUrodzenia = rokUrodzenia;
    }
}
