import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientFlakeIdGeneratorConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.flakeidgen.FlakeIdGenerator;
import com.hazelcast.map.IMap;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class HMapKierowcaCient{
    private HazelcastInstance client;

    public HMapKierowcaCient(HazelcastInstance client)
    {
        this.client = client;
    }
    public boolean save(Kierowca kierowca) throws UnknownHostException {
        FlakeIdGenerator idGenerator = client.getFlakeIdGenerator("idGenerator");
        Map<Long, Kierowca> kierowcy = client.getMap("kierowcy");

        Long key1 = idGenerator.newId();
        try {
            kierowcy.put(key1, kierowca);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public Map<Long, Kierowca> findAll() throws UnknownHostException {
        IMap<Long, Kierowca> kierowcy = client.getMap( "kierowcy" );

        return kierowcy;
    }

    public Map<Long, Kierowca> findAllUnsigned() throws UnknownHostException {
        IMap<Long, Kierowca> kierowcy = client.getMap( "kierowcy" );
        IMap<Long, Ciezarowka> ciezarowki = client.getMap( "ciezarowki" );

        Collection<Ciezarowka> ciezarowki2 = ciezarowki.values();

        Map<Long, Kierowca> kierowcyUnsigned = new HashMap<Long, Kierowca>();
        for (Map.Entry<Long, Kierowca> entry : kierowcy.entrySet()) {
            boolean znaleziono = false;
            for(Ciezarowka c:ciezarowki2)
            {
                if(c.getKierowca().longValue() == entry.getKey())
                {
                    znaleziono = true;
                    break;
                }
            }
            if(!znaleziono)
                kierowcyUnsigned.put(entry.getKey(),entry.getValue());
        }
        return kierowcyUnsigned;
    }

    public boolean update(Long id, Kierowca kierowca) throws UnknownHostException {
        IMap<Long, Kierowca> kierowcy = client.getMap( "kierowcy" );
        //kierowcy.merge(id,kierowca,);
        try {
            kierowcy.put(id, kierowca);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public Kierowca find(Long id) throws UnknownHostException {
        IMap<Long, Kierowca> kierowcy = client.getMap( "kierowcy" );
        Kierowca kierowca = kierowcy.get(id);

        return kierowca;
    }
    public Collection<Kierowca> findAllPredicate(String miejscowosc, Long rok) throws UnknownHostException {
        IMap<Long, Kierowca> kierowcy = client.getMap( "kierowcy" );

        LinkedList<Kierowca> collect = kierowcy.values().stream().filter(x -> x.getMiejscowosc().equals(miejscowosc) && x.getRokUrodzenia() > rok).collect(Collectors.toCollection(LinkedList::new));

        return collect;
    }
}
