import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientFlakeIdGeneratorConfig;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.flakeidgen.FlakeIdGenerator;
import com.hazelcast.map.IMap;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class HMapCiezarowkaCient {
    private HazelcastInstance client;

    public HMapCiezarowkaCient(HazelcastInstance client)
    {
        this.client = client;
    }
    public boolean save(Ciezarowka ciezarowka) throws UnknownHostException {
        FlakeIdGenerator idGenerator = client.getFlakeIdGenerator("idGenerator");
        Map<Long, Ciezarowka> ciezarowki = client.getMap("ciezarowki");

        Long key1 = idGenerator.newId();
        try {
            ciezarowki.put(key1, ciezarowka);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            client.getLifecycleService().shutdown();
            return false;
        }
        return true;
    }

    public Map<Long, Ciezarowka> findAll(String garaz) throws UnknownHostException {
        IMap<Long, Ciezarowka> ciezarowki = client.getMap( "ciezarowki" );

        Map<Long, Ciezarowka> collect = ciezarowki.entrySet().stream()
                .filter(x -> x.getValue().getGaraz().equals(garaz))
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
        return collect;
    }

    public Map<Long, Ciezarowka> findAll() throws UnknownHostException {
        IMap<Long, Ciezarowka> ciezarowki = client.getMap( "ciezarowki" );
        return ciezarowki;
    }

    public boolean remove(Long id) throws UnknownHostException {
        IMap<Long, Ciezarowka> ciezarowki = client.getMap( "ciezarowki" );
        try {
            ciezarowki.remove(id);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    public boolean update(Long id, Ciezarowka ciezarowka) throws UnknownHostException {
        FlakeIdGenerator idGenerator = client.getFlakeIdGenerator("idGenerator");
        Map<Long, Ciezarowka> ciezarowki = client.getMap("ciezarowki");

        try {
            ciezarowki.put(id, ciezarowka);
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
}
